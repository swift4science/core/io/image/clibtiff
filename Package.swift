// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Clibtiff",
    pkgConfig: "libtiff-4",
    products: [
        .library(name: "Clibtiff", targets: ["Clibtiff"])
    ],
    targets: [
        .systemLibrary(name:"clibtiff"),
    ]

)
